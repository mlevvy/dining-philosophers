name := "ActorsOriented"

version := "1.0"

scalaVersion := "2.9.2"

libraryDependencies ++= Seq(
  "com.typesafe.akka" % "akka-actor" % "2.0.3"
)

resolvers +=  "sonatypeSnapshots" at "http://oss.sonatype.org/content/repositories/snapshots"

resolvers +=  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

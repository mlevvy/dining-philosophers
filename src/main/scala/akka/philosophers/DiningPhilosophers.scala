package akka.philosophers

import akka.philosophers.actors.{StatActor, Philosopher, Fork}
import akka.actor.{Props, ActorSystem, ActorRef}
import akka.util.Duration
import java.util.concurrent.TimeUnit

/**
 * User: Michał Lewandowski
 * Date: 07.04.2013
 * Time: 12:21
 */

case class Put() 
case class Take() 
case class ForkGranted() 
case class ForkDenied() 
case class Eat() 
case class Think()
case class ReportEatSucceed(name: String);
case class ReportEatFailedOne(name: String);
case class ReportEatFailedTwo(name: String);
case class ShowReport()
case class ShowFullReport()
case class TimeStamp()


object DiningPhilosophers {
  def main(args:Array[String]){
    val _system = ActorSystem("ProducerConsumerApp")

    val reportActor = _system.actorOf(Props(new StatActor(_system)))

    val fork1 = _system.actorOf(Props(new Fork("Fork 1")))
    val fork2 = _system.actorOf(Props(new Fork("Fork 2")))
    val fork3 = _system.actorOf(Props(new Fork("Fork 3")))
    val fork4 = _system.actorOf(Props(new Fork("Fork 4")))
    val fork5 = _system.actorOf(Props(new Fork("Fork 5")))

    val philosopher1 = _system.actorOf(Props(new Philosopher("Philosopher 1", fork1, fork2,reportActor)));
    val philosopher2 = _system.actorOf(Props(new Philosopher("Philosopher 2", fork2, fork3,reportActor)));
    val philosopher3 = _system.actorOf(Props(new Philosopher("Philosopher 3", fork3, fork4,reportActor)));
    val philosopher4 = _system.actorOf(Props(new Philosopher("Philosopher 4", fork4, fork5,reportActor)));
    val philosopher5 = _system.actorOf(Props(new Philosopher("Philosopher 5", fork5, fork1,reportActor)));

    _system.scheduler.schedule(Duration.create(0, TimeUnit.SECONDS),Duration.create(1, TimeUnit.SECONDS), reportActor, TimeStamp());


    philosopher1 ! Eat();
    philosopher2 ! Eat();
    philosopher3 ! Eat();
    philosopher4 ! Eat();
    philosopher5 ! Eat();

    Thread.sleep(3600000)

    reportActor ! ShowReport()
    reportActor ! ShowFullReport()

  }

}

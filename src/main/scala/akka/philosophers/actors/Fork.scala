package akka.philosophers.actors

import akka.actor.{ActorRef, Actor}
import akka.philosophers.{Put, ForkDenied, ForkGranted, Take}

/**
 * User: Michał Lewandowski
 * Date: 07.04.2013
 * Time: 12:31
 */
class Fork(val name: String) extends Actor {
  def receive: Receive = available


  def available: Receive = {
    case Take() => {
      context.become(unavailable)
    //  println("Fork " + name + " was taken. Sending message back to philosopher.")
      sender ! ForkGranted()
    }
  }

  def unavailable(): Receive = {
    case Take() => {
      sender ! ForkDenied()
    }
    case Put() => {
    //  println("Fork " + name + " is now available")
      context.become(available)
    }
  }
}

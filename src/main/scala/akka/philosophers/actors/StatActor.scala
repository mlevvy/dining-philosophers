package akka.philosophers.actors

import akka.actor.{ActorSystem, ActorRef, Actor}
import akka.philosophers._
import scala.collection.mutable.MutableList;
import scala.collection.mutable.Map
import akka.philosophers.ShowReport
import akka.philosophers.ReportEatSucceed
import akka.philosophers.ReportEatFailedOne
import akka.philosophers.ReportEatFailedTwo
import akka.philosophers.TimeStamp
;


/**
 * User: Michał Lewandowski
 * Date: 20.04.2013
 * Time: 18:02
 */
class StatActor(_system : ActorSystem) extends Actor {
  val listOfSucceeded: MutableList[Map[String, Int]] = MutableList()
  val listOfFailedOne: MutableList[Map[String, Int]] = MutableList()
  val listOfFailedTwo: MutableList[Map[String, Int]] = MutableList()
  val succeeded: Map[String, Int] = Map()
  val failedOne: Map[String, Int] = Map()
  val failedTwo: Map[String, Int] = Map()
  val hashes = "##############################";


  def receive: Receive = {
    case ReportEatSucceed(name) => {
      if (succeeded.contains(name)) {
        succeeded.put(name, succeeded(name) + 1);
      } else {
        succeeded.put(name, 0);
      }
    }

    case ReportEatFailedOne(name) => {
      if (failedOne.contains(name)) {
        failedOne.put(name, failedOne(name) + 1);
      } else {
        failedOne.put(name, 0);
      }
    }

    case ReportEatFailedTwo(name) => {
      if (failedTwo.contains(name)) {
        failedTwo.put(name, failedTwo(name) + 1);
      } else {
        failedTwo.put(name, 0);
      }
    }

    case ShowReport() => {
      showReport()
    }

    case ShowFullReport() => {
      showFullReport()
    }

    case TimeStamp() => {
      listOfSucceeded.+=(succeeded.clone())
      listOfFailedOne.+=(failedOne.clone())
      listOfFailedTwo.+=(failedTwo.clone())
    }
  }

  def showReport() {
    val failedStringOne = failedOne.view map {
      case (key, value) => key + " failed first time" + value + " times."
    } mkString(hashes + "\n", "\n", "\n" + hashes)

    val failedStringTwo = failedTwo.view map {
      case (key, value) => key + " failed second time" + value + " times."
    } mkString(hashes + "\n", "\n", "\n" + hashes)

    val succeedString = succeeded.view map {
      case (key, value) => key + " succeed " + value + " times. "
    } mkString(hashes + "\n", "\n", "\n" + hashes)

    println(succeedString)
    println(failedStringOne)
    println(failedStringTwo)
  }

  def showFullReport() {
    println("\nList of succeeded: ")
    println(succeeded.keys.mkString(","))
    for (element <- listOfSucceeded) {
      val elementString = element.view map {
        case (key, value) => value
      } mkString("", ",", "\n")
      print(elementString)
    }

    println("\nList of failed one: ")
    println(failedOne.keys.mkString(","))
    for (element <- listOfFailedOne) {
      val elementString = element.view map {
        case (key, value) => value
      } mkString("", ",", "\n")
      print(elementString)
    }


    println("\nList of failed two: ")
    println(failedTwo.keys.mkString(","))
    for (element <- listOfFailedTwo) {
      val elementString = element.view map {
        case (key, value) => value
      } mkString("", ",", "\n")
      print(elementString)
    }
    _system.shutdown()
  }
}

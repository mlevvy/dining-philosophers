package akka.philosophers.actors

import akka.actor.{ActorRef, Actor}
import akka.philosophers._
import akka.util.Duration
import java.util.concurrent.TimeUnit
import akka.philosophers.ForkGranted
import akka.philosophers.Take


/**
 * User: Michał Lewandowski
 * Date: 07.04.2013
 * Time: 12:31
 */
class Philosopher(val name: String, val left: ActorRef, val right: ActorRef, val statActor: ActorRef) extends Actor {
  def receive: Receive = thinking;

  def thinking: Receive = {
    case Think() => {
      println(name + " is now thinking. ")
      eatAfter(5)
    }

    case Eat() => {
      context.become(waitingForFirstResponse);
      left ! Take();
      right ! Take();
    //  println(name + " want to start eating. Send two fork requests.")
    }
  }

  def waitingForFirstResponse: Receive = {
    case ForkGranted() => {
    //  println(name + " received left fork, waiting right fork");
      context.become(waitingForSecondResponse(sender))
    }
    case ForkDenied() => {
      statActor ! ReportEatFailedOne(name);
    //  println(name + " denied first fork, changing state to fork unavailable.")
      context.become(forkUnavailable)
    }
  }



  def waitingForSecondResponse(firstFork:ActorRef): Receive = {
    case ForkGranted() => {
      statActor ! ReportEatSucceed(name);
      eatNow()
    }
    case ForkDenied() => {
      statActor ! ReportEatFailedTwo(name);
      // println(name + " denied second fork, changing state to fork unavailable. Returning fork. Trying again to eat now.")
      firstFork ! Put()
      context.become(thinking)
      eatAfter(0.25);
    }
  }

  def forkUnavailable():Receive = {
    case ForkGranted() => {
     // println(name + " get second fork available, but first fork was unavailable. Trying again to eat now.")
      sender ! Put()
      context.become(thinking)
      eatAfter(0.25);
    }
    case ForkDenied() => {
      //println(name + " get second fork unavailable also, Trying again to eat now.")
      context.become(thinking)
      eatAfter(0.25);
    }
  }

  def eating(): Receive = {
    case Eat() => {
      println(name + " is eating now.")
      thinkAfter(2)
    }
    case Think() =>{
      left ! Put()
      right ! Put()
      println(name + " puts down two forks and will think now.")
      thinkNow()
    }
  }

  def eatNow(){
    context.become(eating)
    self ! Eat()
  }

  def thinkNow(){
    context.become(thinking)
    self ! Think()
  }

  def eatAfter(seconds:Double){
    context.system.scheduler.scheduleOnce(Duration.create(seconds, TimeUnit.SECONDS), self, Eat());
  }

  def thinkAfter(seconds:Double){
    context.system.scheduler.scheduleOnce(Duration.create(seconds, TimeUnit.SECONDS), self, Think());
  }
}
